function Car(posX, posY, radius) {
  this.x = posX;
  this.y = posY;
  this.r = radius;
  this.s = 1; //speed
  this.a = 1; //acceleration
  this.w = 0; //wrap width

  this.show = function() {
    noStroke();
    fill('green');
    ellipse(this.x, this.y, this.r);
  }

  this.accelerate = function(accel) {
    this.a = accel;
  }

  this.wrap = function(wrap) {
    this.w = wrap;
  }

  this.update = function() {
    this.s += this.a;
    this.a -= 1;
    if (this.a < 0) this.a = 0;
    this.x += this.s;
    this.x %= this.w;
  }
}