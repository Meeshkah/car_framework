var car;
var stones = [];
var bck = {};
bck.width = 600;
bck.height = 400;
bck.line_width = 40;
bck.lower_bound = 100;
bck.n_lanes = 4;
bck.upper_bound = bck.lower_bound + bck.line_width * bck.n_lanes;
bck.draw = function(){
  background(51);
  strokeWeight(2);
  stroke('white');
  line(0, bck.lower_bound, bck.width, bck.lower_bound);
  line(0, bck.upper_bound, bck.width, bck.upper_bound);
  strokeWeight(1);
  for (i = 1; i < bck.n_lanes; i++) {
    line(0, bck.lower_bound + bck.line_width * i, bck.width, bck.lower_bound + bck.line_width * i);
  }
  fill('white');
  strokeWeight(0);
  for (i = 0; i < bck.n_lanes; i++) {
    text(i, 5, bck.getY(i));
  }
}
bck.getY = function(lane){
  return bck.lower_bound + bck.line_width * (lane + 0.5);
}

function setup() {
  // frameRate(5);
  createCanvas(bck.width, bck.height);
  car = new Car(0,bck.getY(2), bck.line_width * 0.9);
  car.wrap(bck.width);
}

function mousePressed() {
  if (mouseY > bck.lower_bound && mouseY < bck.upper_bound) {
    var ln;
    for (ln = 0; ln < bck.n_lanes; ln++) {
      if (mouseY >= bck.lower_bound + bck.line_width * ln && mouseY < bck.lower_bound + bck.line_width * (ln + 1)) break;
    }
    var stone = {};
    stone.x = mouseX;
    stone.y = bck.getY(ln);
    stone.r = bck.line_width * 0.9;
    stones.push(stone);
  }
}

function draw() {
  bck.draw();
  stones.forEach(function (stone) {
    fill('white');
    ellipse(stone.x, stone.y, stone.r);
  });
  car.update();
  car.show();
}
